var socket = io.connect("http://" + document.domain + ":" + location.port);
socket.on('connect',function(){
	console.log("connected");
	//socket.emit('my event',{data: "START"});
});


function start_car(){
	socket.emit('my event',{data: "START"});	
}

function stop_car(){
	socket.emit('my event',{data: "STOP"});	
}

function sliderValue(){
	var horSlid = document.getElementById('slider').value;
	var vertSlid = document.getElementById('vertSlider').value;
	var output = horSlid + 's' + vertSlid + 't';
	console.log(output);
	socket.emit('my event',{data: output});
}

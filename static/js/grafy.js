window.onload = function () {

 var countID = $.cookie('countID');
 for(var i = 1; i <= parseInt(countID); i++){
  var Select = document.getElementById('sel1');
  var text = "Dataset " + i.toString();
  var value = i;
  //Select.options[Select.options.length] = new Option(text, value);
  $('#sel1').append('<option value="' + value + '" onclick="updateDataset('+value+')">' + text + '</option>');
 }


}

function timeOut(){}


function updateDataset(value){
 socket.emit('graphs',{data: value.toString()});

 setTimeout(function(){},1000);
 var data = '';
 socket.on('graphs_js',function(msg){
  console.log(msg.data);
  data = msg.data;

 //var data = $.cookie('dataset');
 //console.log(data);
 var natocenie = msg.data[0].toString().substring(0,4);
 console.log(natocenie);
 var rychlost = msg.data[0].toString().substring(5,9);
 console.log(rychlost);
 console.log(msg.data.length);
 
    var dps = []; // dataPoints
    var chart = new CanvasJS.Chart("chartContainer", {
        title :{
            text: "Natocenie kolies"
        },
        axisY: {
            includeZero: false,
            title:"Natocenie kolies",
            minimum: 1100,
            maximum: 1900,

        },
        axisX: {
            title:"Cas"
        },
        data: [{
            type: "line",
            dataPoints: dps
        }]
    });

    var xVal = 0;
    var yVal = 100;
    var updateInterval = 1000;
    var dataLength = 20; // number of dataPoints visible at any point
    
    var updateChart = function (count) {

        count = count || 1;

        for (var j = 0; j < msg.data.length; j++) {
            yVal =((Math.random() *800)+1100);
            dps.push({
                x: xVal,
                y: parseInt(msg.data[j].toString().substring(0,4))
            });
            xVal++;
        }

        if (dps.length > dataLength) {
            dps.shift();
        }

        chart.render();
    };

    updateChart(dataLength);
    //setInterval(function(){updateChart()}, updateInterval);


    //druhy graf
    var dps2 = []; // dataPoints
    var chart2 = new CanvasJS.Chart("chartContainer2", {
        title :{
            text: "Rychlost"
        },
        axisY: {
            includeZero: false,
            title:"Rychlost",
            minimum: 1500,
            maximum: 1580,

        },
        axisX: {
            title:"Cas"
        },
        data: [{
            type: "line",
            dataPoints: dps2
        }]
    });

    var xVal2 = 0;
    var yVal2 = 100;
    var dataLength2 = 20; // number of dataPoints visible at any point

    var updateChart2 = function (count) {

        count = count || 1;

        for (var j = 0; j < msg.data.length; j++) {
            yVal2 =((Math.random() *80)+1500);
            dps2.push({
                x: xVal2,
                y: parseInt(msg.data[j].toString().substring(5,9))
            });
            xVal2++;
        }

        if (dps2.length > dataLength2) {
            dps2.shift();
        }

        chart2.render();
    };

    updateChart2(dataLength2);
    //setInterval(function(){updateChart2()}, updateInterval);
});

}
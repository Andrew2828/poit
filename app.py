from flask import Flask, render_template, make_response
from flask_socketio import SocketIO, emit
from threading import Thread
import serial
import time
import RPi.GPIO as IO
import MySQLdb

IO.setwarnings(False)

IO.setmode (IO.BCM)

IO.setup(5,IO.IN)
IO.setup(6,IO.IN)
IO.setup(13,IO.IN)
IO.setup(19,IO.IN)

carState = ''
async_mode = None
graphState = ''
maxID = ''

app = Flask(__name__)
app.config['DEBUG'] = True
socketio = SocketIO(app, async_mode=async_mode)

ser=serial.Serial("/dev/ttyUSB0",115200)
serIR=serial.Serial('/dev/ttyUSB1',19200)

db = MySQLdb.connect(host="localhost",user="root",passwd="1234",db="auto" )
cursor = db.cursor()

@app.route("/")
def index():
    return render_template("index.html")


def background():
    
    while True:
        global carState
        global maxID
        #print(maxID)
        dbData='';
        db = MySQLdb.connect(host="localhost",user="root",passwd="1234",db="auto" )
        cursor1 = db.cursor()
        
        if carState == "START":
            serIR.flushInput()
            #time.sleep(0.01)
            #ser.write("1500s1518t".encode())
            #time.sleep(0.005)
            #serIR.write('check')
            #time.sleep(0.01)
            distance = int(serIR.readline())
            #time.sleep(0.01)
            print(distance)

            if distance < 1000:
                ser.write("1500s1500t".encode())
                dbData="1500s1500t"
            else:
                #print(str(IO.input(5)) + " " + str(IO.input(6)) + " " + str(IO.input(13)) + " " + str(IO.input(19)))
                #ser.write("1500s1518t".encode())
                ser.write("1700s1518t".encode())
                time.sleep(0.01)
                if str(IO.input(5)) == "0":
                    print('PO')
                    #ser.write("1700s1518t".encode())
                    #time.sleep(0.01)
                    dbData="1700s1518t"
                elif str(IO.input(19)) == "0":
                    print('LO')
                    #ser.write("1300s1518t".encode())
                    #time.sleep(0.01)
                    dbData="1300s1518t"
                elif str(IO.input(13)) == "0":
                    print('LI')
                    #ser.write("1400s1518t".encode())
                    #time.sleep(0.01)
                    dbData="1400s1518t"
                elif str(IO.input(6)) == "0":
                    print('PV')
                    #ser.write("1600s1518t".encode())
                    #time.sleep(0.01)
                    dbData="1600s1518t"
                else:
                    #ser.write("1500s1518t".encode())
                    #time.sleep(0.01)
                    print('Rovno')
                    dbData="1500s1518t"
            
        elif carState == "STOP":
            ser.write("1500s1500t".encode())
            time.sleep(0.01)
            dbData="1500s1500t"
            cursor1.close()
            db.close()
            dbData = ''
        else:
            ser.write(carState.encode())
            time.sleep(0.01)
        if(dbData != ''):
            cursor1.execute('INSERT INTO Dataset VALUES("'+str(maxID)+'","'+dbData+'")')
            db.commit()
            time.sleep(0.01)
            dbData = ''
        
            
        

@app.route("/run")
def run():
    thread = Thread(target=background)
    thread.start()
    return render_template("run.html")

@socketio.on('my event')
def connect_handler(json):
    global carState
    carState = json['data']
    cursor.execute("select max(JAZDA_ID) from Dataset")
    countID1 = cursor.fetchall()
    global maxID;
    maxID = int(countID1[0][0])+1;
    serIR.flush()
    print('received json: ' + carState)

@socketio.on('graphs')
def connect_handler(json):
    resp1 = make_response(render_template("graf.html"))
    global graphState
    graphState = json['data']
    print('received graphState json: ' + graphState)
    cursor.execute("SELECT HODNOTA FROM Dataset WHERE JAZDA_ID="+graphState)
    data = cursor.fetchall()
    print(data)
    resp1.set_cookie('dataset', "posielam?")
    socketio.emit('graphs_js',{'data':data})

    return resp1

@app.route("/graf")
def graf():
    resp = make_response(render_template("graf.html"))
    cursor.execute("select max(JAZDA_ID) from Dataset")
    countID = cursor.fetchall()
    #print(countID[0][0])
    resp.set_cookie('countID', str(countID[0][0]))
    

    return resp

def startCar():
    print("start")
    ser.write("1500s1550t".encode())


def stopCar():
    print("stop")
    ser.write("1500s1500t".encode())

if __name__ == "__main__":
    socketio.run(app,host="0.0.0.0", port=80, debug=True)
